package mobile;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
//import static org.powermock.api.mockito.PowerMockito.*;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

import org.mockito.*;
import org.powermock.reflect.Whitebox;

import org.junit.Before;
import org.junit.Test;

import com.mobile.Account;
import com.mobile.AccountDao;
import com.mobile.AccountServiceImpl;

public class AccountServiceTest {

	private AccountServiceImpl accountService;
		
	private AccountDao accountDao;
	
	@Before
	public void setup() {
		accountService = AccountServiceImpl.getInstance();	
		accountDao = mock(AccountDao.class);
		Whitebox.setInternalState(accountService, "accountDao", accountDao);
	}
	
	@Test
	public void checkBalanceForExisitngAccount() throws InterruptedException {
		String accountNumber = "12345678";
		double balance = 20.5;
		when(accountDao.getAccount(accountNumber)).thenReturn(new Account(accountNumber, balance));
		double actualBalance = accountService.checkBalance(accountNumber);
		
		assertThat(actualBalance, equalTo(balance));
		
	}
	
	@Test
	public void checkBalanceForNonExistingAccount() throws InterruptedException {
		String accountNumber = "12345678";
		String errorMsg = "Account not found";
		when(accountDao.getAccount(accountNumber)).thenThrow(new IllegalArgumentException(errorMsg));
		
		try {                   
			accountService.checkBalance(accountNumber);
			fail("No exception thrown");
	    } catch (IllegalArgumentException exception) {
	    	assertThat(exception.getMessage(), equalTo(errorMsg));                 
	    }
	}
	
	@Test
	public void topUpBalanceWithPositiveAmount() throws InterruptedException {
		String accountNumber = "12345678";
		double balance = 20.5;
		double amount = 10.5;
		double expectedNewBalance = balance + amount;
		
		when(accountDao.getAccount(accountNumber)).thenReturn(new Account(accountNumber, balance));		
		double newBalance = accountService.topUpBalance(accountNumber, amount);
		
		assertThat(newBalance, equalTo(expectedNewBalance));
	}
	
	@Test
	public void topUpBalanceForNonExistingAccount() throws InterruptedException {
		String accountNumber = "12345678";
		String errorMsg = "Account not found";
		double amount = 20;
		when(accountDao.getAccount(accountNumber)).thenThrow(new IllegalArgumentException(errorMsg));
		
		try {                   
			accountService.topUpBalance(accountNumber, amount);
			fail("No exception thrown");
	    } catch (IllegalArgumentException exception) {
	    	assertThat(exception.getMessage(), equalTo(errorMsg));                 
	    }		
	}
	
	@Test
	public void topUpBalanceWIthNegativeAmount() throws InterruptedException {
		String accountNumber = "12345678";
		double amount = -5;
		String errorMsg = "Amount must be greater than 0";
		
		try {                   
			accountService.topUpBalance(accountNumber, amount);
			fail("No exception thrown");
	    } catch (IllegalArgumentException exception) {
	    	assertThat(exception.getMessage(), equalTo(errorMsg));                 
	    }
		
	}
	
	@Test
	public void topUpBalanceWIthZeroAmount() throws InterruptedException {
		String accountNumber = "12345678";
		double amount = 0;
		String errorMsg = "Amount must be greater than 0";
		
		try {                   
			accountService.topUpBalance(accountNumber, amount);
			fail("No exception thrown");
	    } catch (IllegalArgumentException exception) {
	    	assertThat(exception.getMessage(), equalTo(errorMsg));                 
	    }		
	}

}
