package com.mobile;

public interface AccountService {

	public double checkBalance(String accountNumber) throws InterruptedException;
	
	public double topUpBalance(String accountNumber, double amount) throws InterruptedException;
	
	public void persistAccountDetail() throws Exception;
}
