package com.mobile;

import com.google.common.util.concurrent.AtomicDouble;

public class Account {

	private String accountNumber;
	private AtomicDouble balance;
	
	public Account() {
	}
	
	public Account(String accountNumber, double balance) {
		this.accountNumber = accountNumber;
		this.balance = new AtomicDouble(balance);
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public AtomicDouble getBalance() {
		return balance;
	}

	public void setBalance(AtomicDouble balance) {
		this.balance = balance;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return accountNumber + AccountDao.SEPARATOR + String.format("%.2f", balance.doubleValue());
	}	
	
}