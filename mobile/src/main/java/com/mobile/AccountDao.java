package com.mobile;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

public class AccountDao {
	
	private static final AccountDao INSTANCE = new AccountDao();
	private static final String FILE_NAME = "account.csv";
	public static final String SEPARATOR = ",";
	
	private ConcurrentMap<String, Account> accountDetail = new ConcurrentHashMap<>(); 
	
	private AccountDao(){
		loadAccountDetail();
	}
	
	private void loadAccountDetail() {
		try (Stream<String> stream = Files.lines(Paths.get(ClassLoader.getSystemResource(FILE_NAME).toURI()))) {
			stream.forEach((line) -> {String[] strArray = line.split(SEPARATOR);
									  String accountNumber = strArray[0];
									  double balance = Double.parseDouble(strArray[1]);
									  accountDetail.put(accountNumber, new Account(accountNumber, balance)); 	});
		} catch (IOException | URISyntaxException | NumberFormatException e) {
			e.printStackTrace();
		}
	}
	
	public static AccountDao getInstance() {
		return INSTANCE;
	}
	
	public Account getAccount(String accountNumber) {
		Account account = accountDetail.get(accountNumber);
		if (account == null) {
			throw new IllegalArgumentException("Account not found");
		}
		
		return account;
	}
	
	public void persistAccountDetail() throws IOException, URISyntaxException {
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(ClassLoader.getSystemResource(FILE_NAME).toURI()))) {
			accountDetail.forEach((k,v)->{
				try {					
					writer.write(v.toString() + "\n");
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				}
			});
		}
	}

}