package com.mobile;


public class Main {
	
	private static AccountServiceImpl accountService = AccountServiceImpl.getInstance();
		
	public static void main(String[] args) throws Exception {
		System.out.println(accountService.checkBalance("81357924"));
		System.out.println(accountService.topUpBalance("81357924", 20));
		accountService.persistAccountDetail();
	}	
}
