package com.mobile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class AccountServiceImpl implements AccountService {
	
	private static final int MAX_AVAILABLE = 20;
	private static final int TIME_OUT_IN_SEC = 60;
	
	private final Semaphore available = new Semaphore(MAX_AVAILABLE, true);
		
	private AccountDao accountDao = AccountDao.getInstance();
	
	private static final AccountServiceImpl INSTANCE = new AccountServiceImpl();
	
	private AccountServiceImpl() {
	}
	
	public static AccountServiceImpl getInstance() {
		return INSTANCE;
	}
	
	public double checkBalance(String accountNumber) throws InterruptedException {
		if (available.tryAcquire(TIME_OUT_IN_SEC, TimeUnit.SECONDS)) {
			try {
				return checkBalanceImpl(accountNumber);
			} finally {
				available.release();
			}
		} else {
			throw new RuntimeException("Request time out");
		}
	}
	
	private double checkBalanceImpl(String accountNumber) {		
		Account account = accountDao.getAccount(accountNumber);
		
		return account.getBalance().doubleValue();
	}
	
	public double topUpBalance(String accountNumber, double amount) throws InterruptedException {
		if (available.tryAcquire(TIME_OUT_IN_SEC, TimeUnit.SECONDS)) {
			try {
				return topUpBalanceImpl(accountNumber, amount);
			} finally {
				available.release();
			}			
					
		}  else {
			throw new RuntimeException("Request time out");
		}	
	}	
	
	private double topUpBalanceImpl(String accountNumber, double amount) {
		if (amount <= 0) {
			throw new IllegalArgumentException("Amount must be greater than 0");
		}
		Account account = accountDao.getAccount(accountNumber);
				
		return account.getBalance().addAndGet(amount);
	}
		 
	
	public void persistAccountDetail() throws Exception {
		accountDao.persistAccountDetail();
	}

}
