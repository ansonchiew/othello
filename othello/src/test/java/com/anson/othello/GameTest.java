package com.anson.othello;

import static org.junit.Assert.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import static com.anson.othello.Game.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.anson.othello.Game.Disc;

public class GameTest {

	private Game game;
	
	@Test
	public void initGame() {		
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
				
		expectedBoard[3][3] = Disc.O;
		expectedBoard[3][4] = Disc.X;
		expectedBoard[4][3] = Disc.X;		
		expectedBoard[4][4] = Disc.O;
		
		game = new Game();
		assertThat(game.getBoard(), equalTo(expectedBoard));		
	}
	
	@Test
	public void validInput() {
		game = new Game();
		boolean retVal = game.move("3d");
		assertThat(retVal, is(true));
	}
	
	@Test
	public void invalidInput() {
		game = new Game();
		try {			
			game.move("ab");
			fail("No exception thrown");
		} catch (IllegalArgumentException exception) {
			assertThat(exception.getMessage(), equalTo("Wrong input format."));		    
		}
	}
	
	@Test
	public void availableValidMove() {	
		game = new Game();
		boolean available = game.isValidMoveAvailable();
		
		assertThat(available, is(true));
	}
	
	@Test
	public void invalidMove() {
		game = new Game();
		game.isValidMoveAvailable();
		boolean valid = game.move("5c");	
				
		assertThat(valid, is(false));		
	}	

	@Test
	public void validMove() {
		game = new Game();
		game.isValidMoveAvailable();
		boolean valid = game.move("3d");	
				
		assertThat(valid, is(true));		
	}
	
	private void assertBoard(String[] inputArray, Disc[][] expectedBoard) {
		game = new Game();
		
		for (String input:inputArray) {
			game.isValidMoveAvailable();
			game.move(input);	
		}
		
		assertThat(game.getBoard(), equalTo(expectedBoard));
	}
	
	@Test
	public void flipVerticallyDown() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[2][3] = Disc.X;
		expectedBoard[3][3] = Disc.X;
		expectedBoard[3][4] = Disc.X;
		expectedBoard[4][3] = Disc.X;		
		expectedBoard[4][4] = Disc.O;

		String[] inputArray = {"3d"};
		
		assertBoard(inputArray, expectedBoard);
	}	

	@Test
	public void flipVerticallyUp() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[3][3] = Disc.O;
		expectedBoard[3][4] = Disc.X;
		expectedBoard[4][3] = Disc.X;		
		expectedBoard[4][4] = Disc.X;
		expectedBoard[5][4] = Disc.X;
		
		String[] inputArray = {"6e"};
		
		assertBoard(inputArray, expectedBoard);		
	}
	
	@Test
	public void flipHorizontallyRight() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[3][2] = Disc.X;
		expectedBoard[3][3] = Disc.X;
		expectedBoard[3][4] = Disc.X;
		expectedBoard[4][3] = Disc.X;		
		expectedBoard[4][4] = Disc.O;
				
		String[] inputArray = {"4c"};		
		assertBoard(inputArray, expectedBoard);	
	}
	
	@Test
	public void flipHorizontallyLeft() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
				
		expectedBoard[3][3] = Disc.O;
		expectedBoard[3][4] = Disc.X;
		expectedBoard[4][3] = Disc.X;		
		expectedBoard[4][4] = Disc.X;
		expectedBoard[4][5] = Disc.X;
				
		String[] inputArray = {"5f"};		
		assertBoard(inputArray, expectedBoard);		
	}
	
	@Test
	public void flipDiagonallyRightDown() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[2][2] = Disc.O;
		expectedBoard[2][3] = Disc.X;
		expectedBoard[3][3] = Disc.O;
		expectedBoard[3][4] = Disc.X;		
		expectedBoard[4][3] = Disc.X;
		expectedBoard[4][4] = Disc.O;
		
		String[] inputArray = {"3d", "3c"};
		assertBoard(inputArray, expectedBoard);
	}

	@Test
	public void flipDiagonallyLeftUp() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[3][3] = Disc.O;
		expectedBoard[3][4] = Disc.X;		
		expectedBoard[4][3] = Disc.X;
		expectedBoard[4][4] = Disc.O;
		expectedBoard[4][5] = Disc.X;
		expectedBoard[5][5] = Disc.O;
		
		String[] inputArray = {"5f", "6f"};
		assertBoard(inputArray, expectedBoard);
	}
	
	@Test
	public void flipDiagonallyRightUp() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[3][2] = Disc.X;
		expectedBoard[3][3] = Disc.X;
		expectedBoard[3][4] = Disc.X;		
		expectedBoard[4][2] = Disc.X;
		expectedBoard[4][3] = Disc.O;
		expectedBoard[4][4] = Disc.O;
		expectedBoard[5][1] = Disc.X;
		
		String[] inputArray = {"4c", "5c", "6b"};
		assertBoard(inputArray, expectedBoard);		
	}

	@Test
	public void flipDiagonallyLeftDown() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[2][6] = Disc.X;
		expectedBoard[3][3] = Disc.O;
		expectedBoard[3][4] = Disc.O;		
		expectedBoard[3][5] = Disc.X;
		expectedBoard[4][3] = Disc.X;
		expectedBoard[4][4] = Disc.X;
		expectedBoard[4][5] = Disc.X;
		
		String[] inputArray = {"5f", "4f", "3g"};
		assertBoard(inputArray, expectedBoard);
	}	

	@Test
	public void flipMultipleDiscs() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[2][3] = Disc.X;
		expectedBoard[3][3] = Disc.X;
		expectedBoard[3][4] = Disc.O;		
		expectedBoard[3][5] = Disc.O;
		expectedBoard[4][3] = Disc.X;
		expectedBoard[4][4] = Disc.O;
		expectedBoard[5][4] = Disc.O;
		expectedBoard[6][4] = Disc.O;
		
		String[] inputArray = {"6e", "4f", "3d", "7e"};
		assertBoard(inputArray, expectedBoard);
	}
	
	@Test
	public void moveAtRightBound() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[2][6] = Disc.X;
		expectedBoard[2][7] = Disc.X;
		expectedBoard[3][3] = Disc.O;
		expectedBoard[3][4] = Disc.O;		
		expectedBoard[3][5] = Disc.O;
		expectedBoard[3][6] = Disc.X;
		expectedBoard[4][3] = Disc.X;
		expectedBoard[4][4] = Disc.X;
		expectedBoard[4][5] = Disc.X;
		
		String[] inputArray = {"5f", "4f", "3g", "4g", "3h"};
		assertBoard(inputArray, expectedBoard);
	}	

	@Test
	public void moveAtLeftBound() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[1][2] = Disc.X;
		expectedBoard[2][2] = Disc.X;		
		expectedBoard[3][0] = Disc.X;
		expectedBoard[3][1] = Disc.X;		
		expectedBoard[3][2] = Disc.X;
		expectedBoard[3][3] = Disc.X;
		expectedBoard[3][4] = Disc.X;
		expectedBoard[4][3] = Disc.X;
		expectedBoard[4][4] = Disc.O;
				
		String[] inputArray = {"4c", "3c", "2c", "4b", "4a"};
		assertBoard(inputArray, expectedBoard);
	}

	@Test
	public void moveAtUpperBound() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[0][5] = Disc.X;
		expectedBoard[1][4] = Disc.X;
		expectedBoard[1][5] = Disc.X;
		expectedBoard[2][3] = Disc.X;
		expectedBoard[2][4] = Disc.O;
		expectedBoard[3][3] = Disc.X;
		expectedBoard[3][4] = Disc.O;
		expectedBoard[4][3] = Disc.X;
		expectedBoard[4][4] = Disc.O;
				
		String[] inputArray = {"3d", "3e", "2f", "2e", "1f"};
		assertBoard(inputArray, expectedBoard);
	}

	@Test
	public void moveAtLowerBound() {
		Disc[][] expectedBoard = new Disc[HEIGHT][WIDTH];
		
		expectedBoard[3][3] = Disc.O;
		expectedBoard[3][4] = Disc.X;
		expectedBoard[4][3] = Disc.O;
		expectedBoard[4][4] = Disc.X;
		expectedBoard[5][3] = Disc.O;
		expectedBoard[5][4] = Disc.X;
		expectedBoard[6][2] = Disc.X;
		expectedBoard[6][3] = Disc.O;
		expectedBoard[6][4] = Disc.X;
		expectedBoard[7][3] = Disc.O;
		
		String[] inputArray = {"6e", "6d", "7c", "7d", "7e", "8d"};
		assertBoard(inputArray, expectedBoard);
	}	
	
	@Test
	public void noValidMoveAvailable() {		
		Disc[][] board = new Disc[Game.HEIGHT][Game.WIDTH];
		
		for (int i=0; i<Game.HEIGHT; i++) {
			for (int j=0; j<Game.WIDTH-1; j++) {
				board[i][j] = Disc.O;
			}			
		}
		
		game = new Game();
		game.setBoard(board);
		
		boolean available = game.isValidMoveAvailable();		
		assertThat(available, is(false));
	}
	
	@Test
	public void noValidMoveAvailableForBothPlayers() {		
		Disc[][] board = new Disc[Game.HEIGHT][Game.WIDTH];
		
		for (int i=0; i<Game.HEIGHT; i++) {
			for (int j=0; j<Game.WIDTH-1; j++) {
				board[i][j] = Disc.O;
			}			
		}

		game = new Game();
		game.setBoard(board);
				
		List<Boolean> expectedResults = new ArrayList<>();
		expectedResults.add(false);
		expectedResults.add(false);
		expectedResults.add(false);
		
		List<Boolean> results = new ArrayList<>();
		results.add(game.isValidMoveAvailable());
		results.add(game.isValidMoveAvailable());
		results.add(game.isStarted());
		
		assertThat(results, equalTo(expectedResults));
	}
	
	private void assertResult(Disc[][] board, Map<Disc, Integer> expectedScoreMap) {
		game = new Game();
		game.setBoard(board);		
		game.isValidMoveAvailable();		
		game.printResult();
		
		Map<Disc, Integer> scoreMap = game.getScoreMap();
		assertThat(scoreMap, equalTo(expectedScoreMap));		
	}
	
	@Test
	public void printWinnerOResult() {		
		Disc[][] board = new Disc[Game.HEIGHT][Game.WIDTH];
		
		for (int i=0; i<Game.HEIGHT; i++) {
			for (int j=0; j<Game.WIDTH-1; j++) {
				board[i][j] = Disc.O;
			}			
		}
		
		for (int i=0; i<Game.HEIGHT; i++) {
			board[i][Game.WIDTH-1] = Disc.X;
		}
		
		Map<Disc, Integer> expectedScoreMap = new HashMap<>();
		expectedScoreMap.put(Disc.X, HEIGHT);
		expectedScoreMap.put(Disc.O, HEIGHT*WIDTH-HEIGHT);
		
		assertResult(board, expectedScoreMap);
	}
	
	@Test
	public void printWinnerXResult() {		
		Disc[][] board = new Disc[Game.HEIGHT][Game.WIDTH];
		
		for (int i=0; i<Game.HEIGHT; i++) {
			for (int j=0; j<Game.WIDTH-1; j++) {
				board[i][j] = Disc.X;
			}			
		}
		
		for (int i=0; i<Game.HEIGHT; i++) {
			board[i][Game.WIDTH-1] = Disc.O;
		}
		
		Map<Disc, Integer> expectedScoreMap = new HashMap<>();
		expectedScoreMap.put(Disc.O, HEIGHT);
		expectedScoreMap.put(Disc.X, HEIGHT*WIDTH-HEIGHT);
		
		assertResult(board, expectedScoreMap);
	}

	@Test
	public void printTieResult() {		
		Disc[][] board = new Disc[Game.HEIGHT][Game.WIDTH];
		
		for (int i=0; i<Game.HEIGHT; i++) {
			for (int j=0; j<Game.WIDTH-(WIDTH/2); j++) {
				board[i][j] = Disc.X;
			}			
		}
				
		for (int i=0; i<Game.HEIGHT; i++) {
			for (int j=WIDTH/2; j<Game.WIDTH; j++) {
				board[i][j] = Disc.O;
			}			
		}
		
		Map<Disc, Integer> expectedScoreMap = new HashMap<>();
		expectedScoreMap.put(Disc.O, (HEIGHT*WIDTH)/2);
		expectedScoreMap.put(Disc.X, (HEIGHT*WIDTH)/2);
		
		assertResult(board, expectedScoreMap);
	}
	
	@Test
	public void printResultBeforeEnd() {		
		game = new Game();
		game.printResult();
		
		assertThat(game.getScoreMap().size(), equalTo(0));		
	}	
	
	@Test
	public void fullBoard() {		
		Disc[][] board = new Disc[Game.HEIGHT][Game.WIDTH];
		
		for (int i=0; i<Game.HEIGHT; i++) {
			for (int j=0; j<Game.WIDTH; j++) {
				board[i][j] = Disc.O;
			}			
		}
		
		game = new Game();
		game.setBoard(board);
		
		List<Boolean> expectedResults = new ArrayList<>();
		expectedResults.add(false);
		expectedResults.add(false);
						
		List<Boolean> results = new ArrayList<>();		
		results.add(game.isValidMoveAvailable());
		results.add(game.isStarted());
		
		assertThat(results, equalTo(expectedResults));		
	}
	
}