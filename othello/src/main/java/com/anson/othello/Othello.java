package com.anson.othello;

import java.util.Scanner;

/**
 * @author Anson Chiew
 * 
 */
public class Othello {

	public static void main(String[] args) {
		Game game = new Game();
		System.out.println("Game started");
		game.printBoard();
		Scanner in = null;
		String input = null;
		while (game.isStarted()) {
			if (game.isValidMoveAvailable()) {
				System.out.print("Player '" + game.getTurn() + "' move:");
				in = new Scanner(System.in);
				input = in.next();
				try {
					if (!game.move(input)) {
						System.out.println("Invalid move. Please try again.");
					} else {
						game.printBoard();
					}
				} catch (IllegalArgumentException ex) {
					System.out.println(ex.getMessage());
					System.out.println("Please try again.");
				}
			} else {
				if (game.getTurn() != null) {
					System.out.println("No valid move available for player '" + game.getTurn().flip() + "'.");
				}	
			}
		}
		
		System.out.println("No further moves available.");
		game.printResult();
	}

}