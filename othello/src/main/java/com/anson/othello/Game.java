package com.anson.othello;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Anson Chiew
 * 
 */
public class Game {

	public static final int HEIGHT = 8;
	public static final int WIDTH = 8;	
	
	private Disc[][] board = new Disc[HEIGHT][WIDTH]; 
	
	private static final Disc START_TURN = Disc.X;
	
	private static final String START_PATTERN = "^[";
	private static final String MID_PATTERN = "][";
	private static final String END_PATTERN = "]$";
	
	private static final int MAX_NO_VALID_MOVE = 2;
	
	private Disc turn;
	
	private String xLabels;
	private String xRange;
	private String yRange;
	
	private String letterDigitPattern;
	private String digitLetterPattern;
	
	private int startAscii;
	private int endAscii;
	
	private boolean started;
	
	private int noValidMoveCount;
	
	private Disc winner;
	
	private Map<Disc, Integer> scoreMap = new HashMap<>();
	
	public Game() {
		init();
	}
	
	private void init() {
		board[3][3] = Disc.O;
		board[3][4] = Disc.X;
		board[4][3] = Disc.X;		
		board[4][4] = Disc.O;
		
		started = true;
		turn = START_TURN;
		
		// construct labels for the axis
		StringBuilder sb = new StringBuilder();
				
		startAscii = (int)'a';
		endAscii = startAscii + WIDTH - 1;
		for (char ch='a'; ch<=(char)endAscii; ch++) {
			sb.append(ch);
		}		
		xLabels = sb.toString();
		
		xRange = "a-" + xLabels.charAt(xLabels.length()-1);
		yRange = "1" + "-" + HEIGHT;
				
		letterDigitPattern = START_PATTERN + xRange + MID_PATTERN + yRange + END_PATTERN;
		digitLetterPattern = START_PATTERN + yRange + MID_PATTERN + xRange + END_PATTERN;		
	}
	
	public boolean move(String input) {
		input = input.toLowerCase();
		if (input.matches(letterDigitPattern) || input.matches(digitLetterPattern)) {
			int x, y = 0;
			int indexOfLetter, indexOfDigit = 0;
			if (input.matches(letterDigitPattern)) {
				indexOfLetter = 0;
				indexOfDigit = 1;
				
			} else {
				indexOfLetter = 1;
				indexOfDigit = 0;
			}
			
			x = (int)(input.charAt(indexOfLetter)) - startAscii; 
			y = Character.getNumericValue(input.charAt(indexOfDigit)) - 1;
			
			return move(x, y, true);
		}
		
		throw new IllegalArgumentException("Wrong input format.");
	}

	/** 
	*  To be called before making a move. If no valid move is available, flip the turn if the board is not full. 
	* 
	*  @return              true if valid move is available 
	*/
	public boolean isValidMoveAvailable() {
		int discCount = 0;
		for (int i=0; i<HEIGHT; i++){
			for (int j=0; j<WIDTH; j++) {
				if (board[i][j] == null) {
					if (move(j, i, false)) {
						noValidMoveCount = 0;
						return true;
					}
				} else {
					discCount++;
				}
			}
		}
		
		// board is full
		if (discCount == WIDTH * HEIGHT) {
			turn = null;
			started = false;
		} else {
			turn = turn.flip();	
			noValidMoveCount++;
			if (noValidMoveCount == MAX_NO_VALID_MOVE) {
				started = false;
			}
		}		
		return false;
	}
	
	/** 
	*  Check if the move is valid. Flip the discs only when flip is true. 
	* 
	*  @param    x          column in the board array 
	*  @param    y          row in the board array 
	*  @param    flip       true if it is a move; false if it is checking for valid move 
	* 
	*  @return              true if the move is valid 
	*/
	private boolean move(int x, int y, boolean flip) {
		boolean valid = false;
		List<Point> flipList = null;
		if (board[y][x] == null) {
			if (flip) {
				flipList = new ArrayList<>();
			}
					
			for (int dx = -1; dx <= 1; dx++) {
				for (int dy = -1; dy <= 1; dy++) {
					int nextX = x + dx;
					int nextY = y + dy;
					
				    // skip out of bound, empty or same disc
					if (nextX < 0 || nextX > WIDTH-1 || 
						nextY < 0 || nextY > HEIGHT-1 || 
						board[nextY][nextX] == null || 
						board[nextY][nextX] == turn) {
						
						continue;
					}
								
					boolean found = false;
					if (flip) {
						flipList.clear();
						flipList.add(new Point(nextX, nextY));
					}
										
					// check along the same direction to look for same disc
					while (!found) {
						nextX += dx;
						nextY += dy;
						if (nextX < 0 || nextX > WIDTH-1 || 
							nextY < 0 || nextY > HEIGHT-1 ||
							board[nextY][nextX] == null) {
							break;
						} 
						
						if (board[nextY][nextX] == turn) {
							found = true;
							valid = true;						
							if (flip) {
								board[y][x] = turn;
								flip(flipList);
							} else { // skip exploring other directions for valid move checking
								return valid;
							}
						} else {
							if (flip) {
								flipList.add(new Point(nextX, nextY));
							}
						}						
					}					
				}				
			}
		}
		if (valid && flip) {
			turn = turn.flip();
		}
		return valid;
	}
	
	private void flip(List<Point> flipList) {
		for (Point point:flipList) {
			board[point.y][point.x] = board[point.y][point.x].flip(); 
		}
	}
			
	public void printBoard() {
		Disc disc = null;
		String output = null;
		for (int i=0; i<HEIGHT; i++) {
			System.out.print((i+1) + " ");
			for (int j=0; j<WIDTH; j++) {
				disc = board[i][j];
				output = disc == null ? "-" : disc.toString();
				System.out.print(output);
			}
			System.out.println();
		}
		System.out.println("  " + xLabels);		
	}
	
	public void printResult() {
	
		int xCount = 0;
		int oCount = 0;
		
		if (!started) {
			for (int i=0; i<HEIGHT; i++){
				for (int j=0; j<WIDTH; j++) {
					if (board[i][j] == Disc.X){
						xCount++;
					} else if (board[i][j] == Disc.O) {
						oCount++;
					}
				}
			}
			
			scoreMap.put(Disc.X, xCount);
			scoreMap.put(Disc.O, oCount);
			
			if (xCount == oCount) {
				System.out.println("Tie (" + oCount + " vs " + xCount + ")");
			} else {
				if (xCount > oCount) {
					winner = Disc.X;
				} else if (xCount < oCount) {
					winner = Disc.O;
				}
				System.out.println("Player '" + winner + "' wins (" + scoreMap.get(winner) + " vs " + scoreMap.get(winner.flip()) + ")");	
			}			
			
		} else {
			System.out.println("Game is still in progress...");
		}
	}	
		
	public Disc getTurn() {
		return turn;
	}

	public Disc[][] getBoard() {
		return board;
	}
	
	public void setBoard(Disc[][] board) {
		this.board = board;
	}
	
	public boolean isStarted() {
		return started;
	}
		
	public Map<Disc, Integer> getScoreMap() {
		return scoreMap;
	}
	
	public enum Disc {
		X, O;
		
		public Disc flip() {
			if (this == X) {
				return O;
			} else {
				return X;
			}
		}
	}
	
}