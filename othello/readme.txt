-------------------------------
Building the project with Maven
-------------------------------

Ensure Maven is installed in the machine.
In Windows command prompt or Linux/Unix/Mac terminal, navigate to the root folder of the project and enter the following command:

mvn clean install

The execution jar file will be created in target folder: othello-0.0.1-SNAPSHOT.jar
Unit tests will be executed and coverage reports will be generated in target/jacoco-ut folder.

------------------------
Running the Application
------------------------

In Windows command prompt or Linux/Unix/Mac terminal, navigate to the target folder of the project and enter the following command:

java -jar othello-0.0.1-SNAPSHOT.jar

---------
Unit Test
---------

Use cases for moves to flip the pieces for all the 8 directions are covered.
Use cases for moves at the 4 boundaries of the board are also covered.

